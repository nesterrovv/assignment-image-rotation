#include "struct_of_bmp_file.h"
#include "bmp_manipulator.h"
#include "file_manipulator.h"
#include "rotation_of_image.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    (void) argc;
    (void) argv;
    if (argc != 3) {
        printf("Incorrect number of arguments. 3 is necessary. (1st is used for starting a program)");
        printf("You need write input.bmp and output.bmp, separated by 1 space");
        return 1;
    }
    const char *path_to_old_file = argv[1];
    const char *path_to_new_file = argv[2];
    printf("Input file with initial image is: '%s' \n", path_to_old_file);
    printf("Output file with saved image is: '%s' \n", path_to_new_file);
    struct image image = {0};
    FILE * old_file = NULL;
    enum result_of_opening status_of_opening = read_file(&old_file, path_to_old_file);
    if(print_result_of_opening(status_of_opening)){
        return status_of_opening;
    }
    enum result_of_reading result_of_reading = read_data_from_bmp(old_file, &image);
    close_file(old_file);
    if(print_reading_result(result_of_reading)){
        return result_of_reading;
    }
    struct image rotated_image = rotate_image(image); // rotate image
    FILE *new_file = NULL;
    enum result_of_opening result_of_writing = write_file(&new_file, path_to_new_file);
    if(print_result_of_opening(result_of_writing)){
        return result_of_writing;
    }
    uint8_t status_of_writing = write_data_to_bmp(new_file,&rotated_image);
    close_file(new_file);
    if(print_writing_result(status_of_writing)){
        return status_of_writing;
    }
    // free 2 images from memory (old image and new rotated image too)...
    free_image(&image);
    free_image(&rotated_image);
    printf("Memory was freed successfully.\n");
    return 0;
}
