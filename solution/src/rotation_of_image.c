//
// Created by Ivan Nesterov on 12/27/2021.
//

#include "../include/rotation_of_image.h"
#include <stdlib.h>

struct image rotate_image(struct image const rotatable_image) {
    struct image new_image = {.height = rotatable_image.width,
                              .width = rotatable_image.height};
    new_image.data = malloc(rotatable_image.height * rotatable_image.width * sizeof(struct pixel));
    for (uint64_t column = 0; column < rotatable_image.height; column++) {
        for (uint64_t row = 0; row < rotatable_image.width; row++) {
            new_image.data[row * rotatable_image.height + (rotatable_image.height - 1 - column)] = rotatable_image.data[column * rotatable_image.width + row];
        }
    }
    return new_image;
}
